import React from "react";
import { IoClose } from "react-icons/io5";
function SubjectPop(props) {
  return (
    <div className="sectionSubjectPop" onClick={props.toggle}>
      <div
        className="subjectPop" /* onClick={() => props.popSubject(props.id)} */
      >
        <div className="subLeftPop">
          <div className="sub">
            <h2>{props.name}</h2>
            <p>{props.description}</p>
            <button
              className="btnDeletePop"
              onClick={() => props.deletePop(props.id)}
            >
              Delete
            </button>
          </div>
          <div className="exit">
            <button onClick={props.toggle}>{IoClose()}</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SubjectPop;
