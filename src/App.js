import React from "react";
import "./App.css";
import Header from "./Header";
import Form from "./Form";
import Subject from "./Subject";
import SubjectPop from "./SubjectPop";
import { nanoid } from "nanoid";
function App() {
  const [formData, setFormData] = React.useState({
    name: "",
    description: "",
  });
  const [subjects, setSubjects] = React.useState(
    () => JSON.parse(localStorage.getItem("Nots")) || []
  );
  const [edite, setEdite] = React.useState(false);
  const [currentId, setCurrentId] = React.useState("");
  const [model, setModel] = React.useState(false);
  console.log(model);
  const [popElement, setPopElement] = React.useState([]);

  React.useEffect(() => {
    localStorage.setItem("Nots", JSON.stringify(subjects));
  }, [subjects]);

  function handleChange(event) {
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        [event.target.name]: event.target.value,
      };
    });
  }

  function handleSubmit(e) {
    //e.preventDefault();
    if (formData.name && formData.description) {
      const subject = {
        name: formData.name,
        description: formData.description,
        id: nanoid(),
      };
      setSubjects((prevSubjects) => {
        return [...prevSubjects, subject];
      });
    }
  }

  function handleCopy(id) {
    const elementToAdd = subjects.filter((subject) => subject.id === id);
    const elementToBeAdd = {
      name: elementToAdd[0].name,
      description: elementToAdd[0].description,
      id: nanoid(),
    };
    setSubjects((prevSubjects) => [...prevSubjects, elementToBeAdd]);
  }

  function handleRemove(e, id) {
    e.preventDefault();
    toggleModel();
    console.log(id);
    const popElement = subjects.filter((subject) => subject.id === id);
    console.log(popElement);
    setPopElement(popElement);
  }

  function handleRemovePop(id) {
    toggleModel();
    console.log(id);
    setSubjects(
      subjects.filter((subject) => {
        return subject.id !== id;
      })
    );
  }

  const renderPopElement = popElement.map((subject, index) => (
    <SubjectPop
      key={index}
      id={subject.id}
      name={subject.name}
      description={subject.description}
      deletePop={handleRemovePop}
      toggle={toggleModel}
    />
  ));

  function handleEdite(id, name, description) {
    setEdite(true);
    setCurrentId(id);
    console.log(name, description);
    setFormData({
      name: name,
      description: description,
    });
  }

  function upDate(e, id) {
    //e.preventDefault();
    console.log(subjects);
    setEdite(false);
    const subjectUpDated = {
      name: formData.name,
      description: formData.description,
      id: nanoid(),
    };
    const state = subjects.map((subject) =>
      subject.id === id ? subjectUpDated : subject
    );
    console.log(state);
    setSubjects(state);
  }

  const allElements = subjects.map((subject, index) => (
    <Subject
      key={index}
      id={subject.id}
      name={subject.name}
      description={subject.description}
      delete={handleRemove}
      copy={handleCopy}
      edite={handleEdite}
    />
  ));

  function toggleModel() {
    setModel(!model);
  }
  return (
    <>
      <Header length={subjects.length} />
      <div className="app">
        <main>
          <section>{allElements}</section>
          <Form
            onSubmitEdite={upDate}
            currentId={currentId}
            onSubmit={handleSubmit}
            onChange={handleChange}
            name={formData.name}
            description={formData.description}
            editeClicked={edite}
          />
        </main>
      </div>
      {model && <div>{renderPopElement}</div>}
    </>
  );
}

export default App;
