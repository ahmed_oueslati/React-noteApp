import React from "react";

function Form(props) {
  return (
    <>
      <form>
        <input
          className="inputName"
          type="text"
          placeholder="Name"
          name="name"
          onChange={props.onChange}
          value={props.name}
        />
        <textarea
          placeholder="Description..."
          name="description"
          onChange={props.onChange}
          value={props.description}
        ></textarea>
        {props.editeClicked ? (
          <button
            className="btnEdit"
            onClick={(event) => props.onSubmitEdite(event, props.currentId)}
          >
            Edit
          </button>
        ) : (
          <button className="btnAdd" onClick={props.onSubmit}>
            Add
          </button>
        )}
      </form>
    </>
  );
}

export default Form;
