import React from "react";

function Header(props) {
  return (
    <header className="">
      <h1>
        Taki<span>Academy</span>
      </h1>
      {props.length === 0 ? (
        <p>You have no notes</p>
      ) : (
        <p>
          you have {props.length} note{props.length > 1 ? "s" : ""}
        </p>
      )}
    </header>
  );
}

export default Header;
