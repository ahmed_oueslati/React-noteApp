import React from "react";
import { AiFillEdit } from "react-icons/ai";
import { AiFillCopy } from "react-icons/ai";
function Subject(props) {
  return (
    <div className="subject">
      <div className="subLeft">
        <h2>{props.name}</h2>
        <p>{props.description}</p>
        <button
          className="btnDelete"
          onClick={(e) => props.delete(e, props.id)}
        >
          Delete
        </button>
      </div>
      <div className="btns">
        <button
          className="btnEdite"
          onClick={() => props.edite(props.id, props.name, props.description)}
        >
          {AiFillEdit()}
        </button>
        <button className="btnCopy" onClick={() => props.copy(props.id)}>
          {AiFillCopy()}
        </button>
      </div>
    </div>
  );
}

export default Subject;
